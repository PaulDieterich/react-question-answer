import React, {Component} from 'react';

import './question.css';


class Question extends Component{
   
      render() {
      
        return (
            <div className=" container card">
                <div>
                    <p>
                     {this.props.question}
                    </p>
                </div>
                    <ul>
                        {this.props.answers.map((a,i) => 
                        <li key={i} id={i} value={a} onClick={this.props.guess}>{a} </li>)}
                    </ul>
                   <button className="btn m-2 btn-primary btn-xl" onClick={this.props.newQueston}>  new Question</button>
            </div>
          
        )
      }
    }
    export default Question;