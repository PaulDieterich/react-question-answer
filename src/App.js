import React, { Component } from 'react';
import './App.css';
import Question from './Components/question/question';
import Info from './Components/info/info'
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios';

class App extends Component {
constructor(props){
  super(props);
  this.state = {
    question: '',
    answers: [],
    correct_answer:'',
    worngCount: 0
  }
this.handelNewQuestion = this.handelNewQuestion.bind(this);
this.guessTheAnswer = this.guessTheAnswer.bind(this)
}
 
  componentDidMount() {
    axios.get(`https://opentdb.com/api.php?amount=1`)
      .then(res => {
        let question =res.data.results[0].question;
        let answers = [];
        let correct_answerArray =[];
        let correct_answer = res.data.results[0].correct_answer;
        correct_answerArray.push(correct_answer);
        const incorrect_answers = res.data.results[0].incorrect_answers;
        answers = incorrect_answers.concat(correct_answerArray);
        answers.sort();
        this.setState({question});
        this.setState({ answers });
        this.setState({correct_answer});
    }).catch(function(err){
        return err;
    })
  }
 
  guess(a){
      console.log(a)
      console.log(this.state.correct_answer)
    if(a === this.state.correct_answer){
        alert("correct")
    }else{
        alert("wrong")
        this.setState((prevState,props) =>{
          return{
            wrongCount: prevState.wrongCount + 1
          }
         
        })
        console.log(wrongCount);
    }
    };
   handelNewQuestion(){
    this.componentDidMount();
   }

  render() {
    return (
      <div className="App">
        <Question 
           question = {this.state.question}
           answers = {this.state.answers}
           correct_answer = {this.state.correct_answer}
           newQueston = {this.handelNewQuestion}
           guess = {this.guess}/>
       <Info/>
      </div>
    );
  }
}

export default App;
